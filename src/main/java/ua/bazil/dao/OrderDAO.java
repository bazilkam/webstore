package ua.bazil.dao;

import ua.bazil.model.CartInfo;
import ua.bazil.model.OrderDetailInfo;
import ua.bazil.model.OrderInfo;
import ua.bazil.model.PaginationResult;

import java.util.List;

/**
 * Created by Bazil on 6/7/2017.
 */
public interface OrderDAO {

    public void saveOrder(CartInfo cartInfo);

    public PaginationResult<OrderInfo> listOrderInfo(int page,
                                                     int maxResult, int maxNavigationPage);

    public OrderInfo getOrderInfo(String orderId);

    public List<OrderDetailInfo> listOrderDetailInfos(String orderId);

}
