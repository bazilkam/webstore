package ua.bazil.dao;

import ua.bazil.entity.Product;
import ua.bazil.model.PaginationResult;
import ua.bazil.model.ProductInfo;

/**
 * Created by Bazil on 6/7/2017.
 */
public interface ProductDAO {



    public Product findProduct(String code);

    public ProductInfo findProductInfo(String code) ;


    public PaginationResult<ProductInfo> queryProducts(int page,
                                                       int maxResult, int maxNavigationPage  );

    public PaginationResult<ProductInfo> queryProducts(int page, int maxResult,
                                                       int maxNavigationPage, String likeName);

    public void save(ProductInfo productInfo);

}
