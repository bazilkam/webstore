package ua.bazil.dao;

import ua.bazil.entity.Account;

/**
 * Created by Bazil on 6/7/2017.
 */
public interface AccountDAO {


    public Account findAccount(String userName );

}
