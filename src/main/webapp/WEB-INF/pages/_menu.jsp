<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8"%>

<%@ taglib uri="http://www.springframework.org/security/tags" prefix="security" %>


    <div class="bar">
        <div class="filter">
            <button class="action filter__item filter__item" data-filter="*">
                <a href="${pageContext.request.contextPath}/">Home</a>
            </button>

            <button class="action filter__item filter__item" data-filter="*">
            <a href="${pageContext.request.contextPath}/productList">
                Product List
            </a>
            </button>

            <button class="action filter__item filter__item" data-filter="*">
            <a href="${pageContext.request.contextPath}/shoppingCart">
                My Cart
            </a>
            </button>

            <button class="action filter__item filter__item" data-filter="*">
            <security:authorize  access="hasAnyRole('ROLE_MANAGER','ROLE_EMPLOYEE')">
                <a href="${pageContext.request.contextPath}/orderList">
                    Order List
                </a>

            </security:authorize>
            </button>


            <security:authorize  access="hasRole('ROLE_MANAGER')">
            <button class="action filter__item filter__item--selected" data-filter="*">
                <a href="${pageContext.request.contextPath}/product">
                    Create Product
                </a>
            </button>
            </security:authorize>
            <jsp:include page="_header.jsp" />
        </div>
        <a href="${pageContext.request.contextPath}/buyProduct?code=${prodInfo.code}">
            <button class="cart">
                <i class="cart__icon fa fa-shopping-cart"></i>
                <span class="text-hidden">Shopping cart</span>
                <span class="cart__count">${myCart.quantityTotal}</span>
            </button>
        </a>
    </div>