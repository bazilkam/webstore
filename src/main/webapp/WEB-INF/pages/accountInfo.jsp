<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="security" %>

<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">

    <title>Account Info</title>

    <link rel="shortcut icon" href="favicon.ico">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">
    <!-- Pixel Fabric clothes icons -->
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/fonts/pixelfabric-clothes/style.css" />
    <!-- General demo styles & header -->
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/demo.css" />
    <!-- Flickity gallery styles -->
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/flickity.css" />
    <!-- Component styles -->
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/component.css" />
    <script src="${pageContext.request.contextPath}/js/modernizr.custom.js"></script>

</head>
<body>

<jsp:include page="_menu.jsp" />
<header class="bp-header cf">
<h1>Account Info</h1>

<div class="account-container">
         <span> User Name: ${pageContext.request.userPrincipal.name}  </span>
        Role:
                <c:forEach items="${userDetails.authorities}" var="auth">
                    <li>${auth.authority }</li>
                </c:forEach>
</div>

</header>
<jsp:include page="_footer.jsp" />

</body>
</html>