<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">

    <title>Bazil webstore</title>
    <link rel="shortcut icon" href="favicon.ico">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">
    <!-- Pixel Fabric clothes icons -->
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/fonts/pixelfabric-clothes/style.css" />
    <!-- General demo styles & header -->
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/demo.css" />
    <!-- Flickity gallery styles -->
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/flickity.css" />
    <!-- Component styles -->
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/component.css" />
    <script src="${pageContext.request.contextPath}/js/modernizr.custom.js"></script>
</head>
<body>


<jsp:include page="_menu.jsp" />


<header class="bp-header cf">
    <span>@copy Vasiliy Kamaev <span class="bp-icon bp-icon-about" data-content="Created by student TNTU SI-31"></span></span>
    <h1>Welcome to Bazil webstore</h1>

    <h2>Contact</h2>

    <p> Phone: +380502988742</p>
    <p>Adress: Azovskiy st. 9</p>
    <p>Email: bazil.kamaev@gmail.com</p>

</header>


<jsp:include page="_footer.jsp" />

</body>
</html>