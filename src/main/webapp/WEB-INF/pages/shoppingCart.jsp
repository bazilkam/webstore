<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@taglib uri="http://www.springframework.org/tags/form" prefix="form"%>

<!DOCTYPE html>
<html>
<head>
    <meta charset="UTF-8">

    <title>Shopping Cart</title>

    <link rel="shortcut icon" href="favicon.ico">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">
    <!-- Pixel Fabric clothes icons -->
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/fonts/pixelfabric-clothes/style.css" />
    <!-- General demo styles & header -->
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/demo.css" />
    <!-- Flickity gallery styles -->
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/flickity.css" />
    <!-- Component styles -->
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/component.css" />
    <script src="${pageContext.request.contextPath}/js/modernizr.custom.js"></script>

</head>
<body>
<jsp:include page="_menu.jsp" />
<div class="view">
<div class="component">





<fmt:setLocale value="en_US" scope="session"/>

<h1>My Cart</h1>

<c:if test="${empty cartForm or empty cartForm.cartLines}">
    <h2>There is no items in Cart</h2>
    <a href="${pageContext.request.contextPath}/productList">Show
        Product List</a>
</c:if>

<c:if test="${not empty cartForm and not empty cartForm.cartLines   }">
    <form:form method="POST" modelAttribute="cartForm"
               action="${pageContext.request.contextPath}/shoppingCart">

        <c:forEach items="${cartForm.cartLines}" var="cartLineInfo"
                   varStatus="varStatus">
<div class="grid__item">
    <div class="slider">
        <div class="slider__item">
            <img class="product-image" src="${pageContext.request.contextPath}/productImage?code=${cartLineInfo.productInfo.code}" />
        </div>
    </div>
        <div class="meta">
            Code: ${cartLineInfo.productInfo.code}
            <form:hidden path="cartLines[${varStatus.index}].productInfo.code" />

            <h3 class="meta__title"> Name: ${cartLineInfo.productInfo.name}</h3>
            <span class="meta__price">Price: <fmt:formatNumber value="${cartLineInfo.productInfo.price}" type="currency"/></span>

            Quantity: <form:input path="cartLines[${varStatus.index}].quantity" />
             Subtotal: <fmt:formatNumber value="${cartLineInfo.amount}" type="currency"/>
            <a href="${pageContext.request.contextPath}/shoppingCartRemoveProduct?code=${cartLineInfo.productInfo.code}"> Delete </a>
            </div>
    </div>
        </c:forEach>
        <div style="clear: both"></div>
        <input class="button-update-sc" type="submit" value="Update Quantity" />
        <a class="navi-item"
           href="${pageContext.request.contextPath}/shoppingCartCustomer">Enter
            Customer Info</a>
        <a class="navi-item"
           href="${pageContext.request.contextPath}/productList">Continue
            Buy</a>
    </form:form>


</c:if>


<jsp:include page="_footer.jsp" />
    </div>
</div>
<script src="${pageContext.request.contextPath}/js/isotope.pkgd.min.js"></script>
<script src="${pageContext.request.contextPath}/js/flickity.pkgd.min.js"></script>
<script src="${pageContext.request.contextPath}/js/main.js"></script>
</body>
</html>