<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<%@ taglib uri="http://www.springframework.org/security/tags" prefix="security" %>
<!DOCTYPE html>
<html lang="en" class="no-js">
<head>
    <title>Blueprint: Filterable Product Grid</title>
    <link rel="shortcut icon" href="favicon.ico">
    <!-- Font Awesome -->
    <link rel="stylesheet" href="http://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">
    <!-- Pixel Fabric clothes icons -->
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/fonts/pixelfabric-clothes/style.css" />
    <!-- General demo styles & header -->
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/demo.css" />
    <!-- Flickity gallery styles -->
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/flickity.css" />
    <!-- Component styles -->
    <link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/css/component.css" />
    <script src="${pageContext.request.contextPath}/js/modernizr.custom.js"></script>
</head>
<body>
<fmt:setLocale value="en_US" scope="session"/>
<!-- Bottom bar with filter and cart info -->
<jsp:include page="_menu.jsp" />
<!-- Main view -->
<div class="view">
    <!-- Blueprint header -->
    <header class="bp-header cf">
        <span>@copy Vasiliy Kamaev <span class="bp-icon bp-icon-about" data-content="Created by student TNTU SI-31"></span></span>
        <h1>Welcome to Bazil webstore</h1>
    </header>
    <!-- Grid -->
    <section class="grid grid--loading">
        <!-- Loader -->
        <img class="grid__loader" src="images/grid.svg" width="60" alt="Loader image" />
        <!-- Grid sizer for a fluid Isotope (Masonry) layout -->
        <div class="grid__sizer"></div>
        <!-- Grid items -->


        <c:forEach items="${paginationProducts.list}" var="prodInfo">
        <div class="grid__item">
            <div class="slider">
                <div class="slider__item"><img class="product-image"
                                               src="${pageContext.request.contextPath}/productImage?code=${prodInfo.code}" /></div>
            </div>
            <div class="meta">
                <h3 class="meta__title">${prodInfo.name}</h3>
                <span class="meta__brand">${prodInfo.code}</span>
                <span class="meta__price">$ <fmt:formatNumber value="${prodInfo.price}" type="currency"/></span>
            </div>
            <a href="${pageContext.request.contextPath}/buyProduct?code=${prodInfo.code}">
            <button class="action action--button action--buy"><i class="fa fa-shopping-cart"></i><span class="text-hidden">
                        Buy Now</span></button></a>
            <security:authorize  access="hasRole('ROLE_MANAGER')">
                <a style="color:#7c97ff;"
                   href="${pageContext.request.contextPath}/product?code=${prodInfo.code}">
                    Edit Product</a>
            </security:authorize>
        </div>
                    <!-- For Manager edit Product -->

        </c:forEach>
</div>

<c:if test="${paginationProducts.totalPages > 1}">
    <div class="page-navigator">
        <c:forEach items="${paginationProducts.navigationPages}" var = "page">
            <c:if test="${page != -1 }">
                <a href="productList?page=${page}" class="nav-item">${page}</a>
            </c:if>
            <c:if test="${page == -1 }">
                <span class="nav-item"> ... </span>
            </c:if>
        </c:forEach>

    </div>
</c:if>


    </section>
    <!-- /grid-->
</div>
<!-- /view -->
<script src="${pageContext.request.contextPath}/js/isotope.pkgd.min.js"></script>
<script src="${pageContext.request.contextPath}/js/flickity.pkgd.min.js"></script>
<script src="${pageContext.request.contextPath}/js/main.js"></script>
</body>
</html>